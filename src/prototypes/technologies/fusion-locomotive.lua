data:extend(
	{	
		{
			type = "technology",
			name = "kr-fusion-locomotive",
			mod = "Krastorio2-more-locomotives",
			icon = krml_technologies_icons_path .. "fusion-locomotive.png",
			icon_size = 128,
			effects =
			{
				{
					type = "unlock-recipe",
					recipe = "kr-fusion-locomotive"
				}
			},
			prerequisites = {"railway", "fusion-reactor-equipment"},
			unit =
			{
				count = 500,
				ingredients = 
				{
					{"automation-science-pack", 1},
					{"logistic-science-pack", 1},
					{"chemical-science-pack", 1},
					{"production-science-pack", 1}
				},
				time = 60
			}
		}
	}
)
