data:extend(
	{	
		{
			type = "technology",
			name = "kr-antimatter-locomotive",
			mod = "Krastorio2-more-locomotives",
			icon = krml_technologies_icons_path .. "antimatter-locomotive.png",
			icon_size = 128,
			effects =
			{
				{
					type = "unlock-recipe",
					recipe = "kr-antimatter-locomotive"
				}
			},
			prerequisites = {"railway", "kr-antimatter-reactor-equipment"},
			unit =
			{
				count = 650,
				ingredients = 
				{
					{"production-science-pack", 1},
					{"utility-science-pack", 1},
					{"space-science-pack", 1},
					{"matter-tech-card", 1},
					{"advanced-tech-card", 1},
					{"singularity-tech-card", 1}
				},
				time = 60
			}
		}
	}
)
