data:extend(
{	
	{
		type = "item-with-entity-data",
		name = "kr-fusion-locomotive",
		icon = krml_vehicles_icons_path .. "fusion-locomotive.png",
		icon_size = 64,
		subgroup = "transport",
		order = "a[train-system]-g[fusion-locomotive.png]",
		place_result = "kr-fusion-locomotive",
		stack_size = 5
	},
	{
		type = "item-with-entity-data",
		name = "kr-antimatter-locomotive",
		icon = krml_vehicles_icons_path .. "antimatter-locomotive.png",
		icon_size = 64,
		subgroup = "transport",
		order = "a[train-system]-h[antimatter-locomotive.png]",
		place_result = "kr-antimatter-locomotive",
		stack_size = 5
	}
})
