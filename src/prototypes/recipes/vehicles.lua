data:extend(
{
	{
		type = "recipe",
		name = "kr-fusion-locomotive",
		energy_required = 60,
		enabled = false,
		allow_productivity = true,
		ingredients =
		{
			{"kr-nuclear-locomotive", 1},
			{"ai-core", 4},
			{"steel-gear-wheel", 20},
			{"fusion-reactor-equipment", 4},
			{"rare-metals", 80}
		},
		result = "kr-fusion-locomotive"
	},
	{
		type = "recipe",
		name = "kr-antimatter-locomotive",
		energy_required = 60,
		enabled = false,
		allow_productivity = true,
		ingredients =
		{
			{"kr-nuclear-locomotive", 1},
			{"ai-core", 50},
			{"imersium-gear-wheel", 80},
			{"antimatter-reactor-equipment", 16},
			{"rare-metals", 200}
		},
		result = "kr-antimatter-locomotive"
	}
})
