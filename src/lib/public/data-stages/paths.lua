-- -- -- PATHS
-- This paths is meant for be used only in data stages
-----------------------------------------------------------------------------------------------------------------
-- -- GENERIC PATHS
-----------------------------------------------------------------------------------------------------------------
krml_path                                = "__Krastorio2-more-locomotives__/"
krml_prototypes_path                     = krml_path            .. "prototypes/"
-- Prototypes:
krml_entities_prototypes_path            = krml_prototypes_path .. "entities/"
krml_items_prototypes_path               = krml_prototypes_path .. "items/"
krml_recipes_prototypes_path             = krml_prototypes_path .. "recipes/"
krml_technologies_prototypes_path        = krml_prototypes_path .. "technologies/"
-----------------------------------------------------------------------------------------------------------------
-- -- GRAPHICS PATHS
-----------------------------------------------------------------------------------------------------------------
krml_graphic_mod_path                    = krml_path .. "graphics/"
-----------------------------------------------------------------------------------------------------------------
-- Icons paths
krml_icons_path                          = krml_graphic_mod_path .. "icons/"
krml_technologies_icons_path             = krml_graphic_mod_path .. "technologies/"
krml_vehicles_icons_path                 = krml_icons_path       .. "vehicles/"
-----------------------------------------------------------------------------------------------------------------
-- Entities
krml_entities_path                       = krml_graphic_mod_path .. "entities/"
-----------------------------------------------------------------------------------------------------------------
