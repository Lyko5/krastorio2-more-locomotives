---------------------------------------------------------------------------
-- -- -- PRE INITIALIZATION
---------------------------------------------------------------------------
-- -- Global Krastorio 2 - More Locomotives Paths
require("__Krastorio2-more-locomotives__/lib/public/data-stages/paths")
---------------------------------------------------------------------------
-- -- -- CONTENTS INITIALIZATION (data stage)
---------------------------------------------------------------------------
-- -- Adding new entities
require(krml_entities_prototypes_path .. "entities-initialization")
-- -- Adding new items
require(krml_items_prototypes_path .. "items-initialization")
-- -- Adding new recipes
require(krml_recipes_prototypes_path .. "recipes-initialization")
-- -- Adding new technologies
require(krml_technologies_prototypes_path .. "technologies-initialization")
